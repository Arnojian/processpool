#ifndef PROCESSPOOL_H_
#define PROCESSPOOL_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <errno.h>


// 子进程类，m_pid是目标子进程的PID,m_pidefd是父进程与子进程通信用的管道
class process
{
	public:
		process():m_pid(-1){}
	
	public:
		pid_t m_pid ;
		int   m_pipefd[2];
};
template<typename T>
class processpool
{
	private:
		processpool(int listenfd,int processNumber=8);
	public:
		static processpool<T> * create(int listenfd,int processNumber=8)
		{
			if(!m_Instance)
			{
				m_Instance = new processpool<T>(listenfd,processNumber) ;
			}
			return m_Instance ;
		}

		~processpool()
		{
			delete [] m_SubProcess;
		}
		
		void run() ;
	private :
		void setupSigPipe();
		void runParent();
		void runChild();

	private :
		static const int MAX_PROCESS_NUMBER=8 ;  // 进程池的最大子进程数量
		static const int USER_PER_PROCESS=65535 ;//每个子进程最大处理客户数量
		static const int MAX_EVENT_NUMBER=10000;//epoll最多处理的事件数
		int m_ProcessNumber ; // 进程池中的总进程数
		int m_Idx ;// 子进程在进程池中的编号，父进程为-1
		int m_Epollfd ;// 每个进程都有一个epoll内核事件表
		int m_Listenfd ;//监听socket;
		int m_Stop ;
		process *m_SubProcess ;
		static processpool<T> *m_Instance ;
};
//初始化  静态变量
template <typename T>
processpool<T> * processpool<T>::m_Instance = NULL ;

//用于处理信号的管道，以实现统一事件源
static int sigPipeFd[2] ;

//设置非阻塞
static int setNonBlock(int fd)
{
	int oldOption = fcntl(fd,F_GETFL) ;
	int newOption = oldOption | O_NONBLOCK ;
	fcntl(fd,F_SETFL,newOption) ;
	return oldOption ;
}

// add the socket to epollfd
static void addFd(int epollFd,int fd)
{
	epoll_event event ;
	event.data.fd = fd ;
	event.events = EPOLLIN | EPOLLET;
	epoll_ctl(epollFd,EPOLL_CTL_ADD,fd,&event) ;
	setNonBlock(fd) ;
}
//删除fd从epollFd标识的内核事件源中
static void removeFd(int epollFd,int fd)
{
	epoll_ctl(epollFd,EPOLL_CTL_DEL,fd,NULL) ;
	close(fd) ;
}

static void sigHandler(int sig)
{
	int saveErrno = errno ;
	int msg = sig ;
	send(sigPipeFd[1],(char *)msg,1,0) ;
	errno = saveErrno ;
}


static void addSig(int sig,void (handler)(int),bool restart=true)
{
	struct sigaction sa ;
	memset(&sa,0x00,sizeof(sa)) ;
	sa.sa_handler=handler ;
	if(restart)
	{
		sa.sa_flags |= SA_RESTART ;
	}
	sigfillset(&sa.sa_mask);
	assert(sigaction(sig,&sa,NULL)!=-1) ;
}

template<typename T>
processpool<T>::processpool(int listenFd,int processNumber):m_Listenfd(listenFd),m_ProcessNumber(processNumber),m_Idx(-1),m_Stop(false)
{
	assert( (processNumber >0)&&(processNumber <=MAX_PROCESS_NUMBER)) ;

	m_SubProcess = new process[m_ProcessNumber] ;
	assert(m_SubProcess) ;

	for(int i = 0 ;i<m_ProcessNumber;i++)
	{
		int ret = socketpair(PF_UNIX,SOCK_STREAM,0,m_SubProcess[i].m_pipefd) ;
		assert(ret==0);

		m_SubProcess[i].m_pid = fork() ;
		assert(m_SubProcess[i].m_pid >=0);
		//父进程
		if(m_SubProcess[i].m_pid >0)
		{
			close(m_SubProcess[i].m_pipefd[0]) ;
			continue ;
		}
		else //子进程
		{
			close(m_SubProcess[i].m_pipefd[1]);
			m_Idx=i ;
			break ;
		}
	}
}

template<typename T>
void processpool<T>::setupSigPipe()
{
	m_Epollfd = epoll_create(5) ;
	assert(m_Epollfd != -1);

	int ret = socketpair(PF_UNIX,SOCK_STREAM,0,sigPipeFd) ;
	assert(ret != -1) ;

	setNonBlock(sigPipeFd[1]) ;
	addFd(m_Epollfd,sigPipeFd[0]);

	addSig(SIGCHLD,sigHandler);
	addSig(SIGTERM,sigHandler);
	addSig(SIGINT,sigHandler);
	addSig(SIGPIPE,SIG_IGN);
}

template<typename T>
void processpool<T>::run()
{
	if(m_Idx != -1)
	{
		runChild();
	}
	runParent() ;
}

template<typename T>
void processpool<T>::runChild()
{
	setupSigPipe() ;
	// 1用于读取父进程信息
	int pipeFd = m_SubProcess[m_Idx].m_pipefd[1] ;
	
	addFd(m_Epollfd,pipeFd) ;

	epoll_event events[MAX_EVENT_NUMBER];

	T* users= new T[USER_PER_PROCESS] ;
	assert(users) ;

	int number = 0 ;
	int ret = -1 ;

	while(!m_Stop)
	{
		number = epoll_wait(m_Epollfd,events,MAX_EVENT_NUMBER,-1);
		if(number <0 && errno != EINTR)
		{
			printf("epoll failure\n");
			break ;
		}

		for(int i = 0 ;i<number;i++)
		{
			int sockfd = events[i].data.fd ;
			if(sockfd == pipeFd && events[i].events & EPOLLIN)
			{
				int client = 0 ;
				ret = recv(sockfd,(char *)&client,sizeof(client),0) ;
				if( ((ret <0) && (errno != EAGAIN)) || ret ==0)
				{
					continue ;
				}
				else
				{
					struct sockaddr_in clientAddr ;
					memset(&clientAddr,0x00,sizeof(clientAddr));
					
					socklen_t clientAddrLen = sizeof(clientAddr);

					int connfd = accept(m_Listenfd,(struct sockaddr*)&clientAddr,&clientAddrLen);
					if(connfd <0)
					{
						printf("errno is :%d\n",errno);
						continue ;
					}
					addFd(m_Epollfd,connfd);

					users[connfd].init(m_Epollfd,connfd,clientAddr);
				}
			}
			else if(sockfd == sigPipeFd[0] && events[i].events & EPOLLIN)
			{
				int sig ;
				char signals[1024] ;
				ret = recv(sigPipeFd[0],signals,sizeof(signals),0);
				if(ret <0)
				{
					continue ;
				}
				else
				{
					for(int i =0;i<ret;i++)
					{
						switch(i=signals[i])
						{
							case SIGCHLD:
							{
								pid_t pid ;
								int stat ;
								while(pid = waitpid(-1,&stat,WNOHANG)>0)
								{
									continue ;
								}
								break ;
							}
							case SIGTERM:
							case SIGINT:
							{
								m_Stop = true ;
								break ;
							}
							default:
							{
								break ;
							}
						}
					}
				}

			}
			else if(events[i].events & EPOLLIN)
			{
				users[sockfd].process();
			}
			else
			{
				continue ;
			}
		}
	}

	delete [] users;
	users = NULL;
	close(pipeFd);
	//close(m_Listenfd);
	close(m_Epollfd);
}


template<typename T>
void processpool<T>::runParent()
{
	setupSigPipe() ;
	/*父进程监听m_Listenfd*/
	addFd(m_Epollfd,m_Listenfd);

	epoll_event events[MAX_EVENT_NUMBER];
	int subProcessCounter = 0 ;
	int newConn = 1;
	int number = -1 ;
	int ret = -1 ;

	while(!m_Stop)
	{
		number = epoll_wait(m_Epollfd,events,MAX_EVENT_NUMBER,-1);
		if( (number <0) && (errno != EINTR))
		{
			printf("epoll failure\n");
			break ;
		}
		for(int i = 0 ;i<number;i++)
		{
			int sockfd = events[i].data.fd ;
			if(sockfd == m_Listenfd)
			{
				int i = subProcessCounter ;
				do{
					if(m_SubProcess[i].m_pid != -1)
					{
						break ;
					}
					i = (i+1)%m_ProcessNumber ;

				}while(i!=subProcessCounter);
				
				if(m_SubProcess[i].m_pid==-1)
				{
					m_Stop = true ;
					break ;
				}

				subProcessCounter = (i+1)%m_ProcessNumber;
				send(m_SubProcess[i].m_pipefd[0],(char*)&newConn,sizeof(newConn),0);
				printf("send request to child:%d\n",i) ;

			}
			else if(sockfd == sigPipeFd[0] && events[i].events & EPOLLIN)
			{
				int sig ;
				char signals[1024] ;
				ret = recv(sigPipeFd[0],signals,sizeof(signals),0);
				if(ret <=0)
				{
					continue ;
				}
				else
				{
					for(int i =0;i<ret ;i++)
					{
						switch(signals[i])
						{
							case SIGCHLD:
							{
								pid_t pid ;
								int stat ;
								while( (pid = waitpid(-1,&stat,WNOHANG))>0)
								{
									for(int i = 0 ;i<m_ProcessNumber;i++)
									{
										if(m_SubProcess[i].m_pid==pid)
										{
											printf("child %d join\n",i);
											close(m_SubProcess[i].m_pipefd[0]);
											m_SubProcess[i].m_pid=-1;
										}
									}
								}

								m_Stop = true ;
								for(int i = 0 ;i<m_ProcessNumber;i++)
								{
									if(m_SubProcess[i].m_pid != -1)
									{
										m_Stop = false;
									}
								}
								break ;
							}
							case SIGTERM:
							case SIGINT:
							{
								printf("kill all the child now\n");
								for(int i = 0 ;i< m_ProcessNumber;i++)
								{
									int pid = m_SubProcess[i].m_pid ;
									if(pid != -1)
									{
										kill(pid,SIGTERM);
									}
								}
								break ;
							}
							default:
							{
								break ;
							}
						}// end switch
					}//for
				}// end if(ret )
			}
			else
			{
				continue ;
			}
		}
	}
	close(m_Epollfd);
}

#endif
